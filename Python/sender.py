
import socket
import time
# import numpy as np
import cv2

UDP_IP = "192.168.1.57"  # 57 / 41
UDP_PORT = 4242

vwidth = 32
vheight = 8

print ("UDP target IP:", UDP_IP)
print ("UDP target port:", UDP_PORT)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP

# cap = cv2.VideoCapture('Daft Punk - Harder Better Faster.flv')
cap = cv2.VideoCapture('sample.mp4')

# for sample conversion only:
# video_out = cv2.VideoWriter( "sample.mp4", cv2.VideoWriter_fourcc(*'mp4v'), 20.0, (vwidth, vheight))

while cap.isOpened():
    ret, frame = cap.read()
    if frame is None:
        break
    h = frame.shape[0]
    w = frame.shape[1]
    '''x1 = 100
    x2 = 180
    y1 = 185
    y2 = 175'''
    x1 = 50
    x2 = 50
    y1 = 100
    y2 = 100
    cv2.imshow('frame', frame)
    # crop_img = frame[y1: h - y2, x1:w - x2]
    # cv2.imshow('crop', crop_img)
    # fra = cv2.resize(crop_img, (vwidth, vheight))

    # video_out.write(fra) # for sample conversion only

    sock.sendto(frame, (UDP_IP, UDP_PORT))
    # cv2.imshow('led', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    time.sleep(1/25)

cap.release()
# video_out.release() # for sample conversion only
cv2.destroyAllWindows()
