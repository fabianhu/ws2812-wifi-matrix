#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

#include "secret.h"

#ifndef STASSID
#error "no WIFI data"
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

char str[100];

#define PIN 2

WiFiUDP Udp;
unsigned int localUdpPort = 4242;  // local port to listen on
char incomingPacket[32 * 8 * 3 + 10]; // buffer for incoming packets

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(32, 8, PIN,
                            NEO_MATRIX_TOP     + NEO_MATRIX_LEFT +
                            NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
                            NEO_GRB            + NEO_KHZ800);

const uint16_t colors[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0)
};

void clearScreen() {
  int x = 0;
  int y = 0;
  for (int l = 0; l <= 3 * 8 * 32; l += 3)
  {
    matrix.drawPixel(x++, y, matrix.Color(0, 0, 0)); // GRB order
    if (x >= matrix.width())
    {
      y++;
      x = 0;
    }
  }
  matrix.show();
}


void setup() {
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(255);
  matrix.setTextColor(colors[0]);

  Serial.begin(115200);
  Serial.println();

  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected");

  Udp.begin(localUdpPort);
  sprintf(str, "Now listening at %s:%d UDP   @fab1anhu", WiFi.localIP().toString().c_str(), localUdpPort);
  Serial.printf(str);
  //             Now listening at 192.168.111.155, UDP port 75555
  //             1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
  //                      10        20        30        40        50        60        70        80        90        100   
  clearScreen();
  
// ArduinoOTA.setPort(8266);
ArduinoOTA.setHostname("LED_Matrix");
// ArduinoOTA.setPassword("admin");
  ArduinoOTA.begin();
}

uint32_t ctr = 0;
int xs= matrix.width();

void loop() {
  ArduinoOTA.handle();
  ctr++;
  if (ctr >= 100000)
  {
    //clearScreen();
    matrix.setBrightness(50);
    matrix.fillScreen(0);
    matrix.setCursor(xs, 0);
    
    matrix.print(str);
     if(--xs < -300) {  // the 300 is pixels, not characters
    xs = matrix.width();

    }
    matrix.show();
    delay(80);
  }

  
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    matrix.setBrightness(255);
    ctr = 0; // reset timeout counter
    xs= matrix.width();
    // receive incoming UDP packets
    int len = Udp.read(incomingPacket, 255 * 3 + 3);

    /* // send back a reply, to the IP address and port we got the packet from
      Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
      Udp.write(replyPacket);
      Udp.endPacket();*/

    int x = 0;
    int y = 0;
    for (int l = 0; l <= len; l += 3)
    {
      matrix.drawPixel(x++, y, matrix.Color(incomingPacket[l + 2], incomingPacket[l + 1], incomingPacket[l + 0])); // GRB order
      //Serial.printf("%d: R%d G%d B%d\n", l, incomingPacket[l], incomingPacket[l+1], incomingPacket[l+2]);
      if (x >= matrix.width())
      {
        y++;
        x = 0;
      }
    }
    matrix.show();
  }
}
