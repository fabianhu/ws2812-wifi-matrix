EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ESP:ESP-03 U1
U 1 1 5F739893
P 5100 3725
F 0 "U1" H 5100 4806 50  0000 C CNN
F 1 "ESP-03" H 5100 4715 50  0000 C CNN
F 2 "ESP:ESP-03" H 5100 3825 50  0001 C CNN
F 3 "http://l0l.org.uk/2014/12/esp8266-modules-hardware-guide-gotta-catch-em-all/" H 5100 3825 50  0001 C CNN
	1    5100 3725
	1    0    0    -1  
$EndComp
$Comp
L Device:R PRG1
U 1 1 5F73AADC
P 6325 3425
F 0 "PRG1" V 6118 3425 50  0000 C CNN
F 1 "PROG" V 6209 3425 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6255 3425 50  0001 C CNN
F 3 "~" H 6325 3425 50  0001 C CNN
	1    6325 3425
	0    1    1    0   
$EndComp
$Comp
L Regulator_Switching:TPS560200 U2
U 1 1 5F73B059
P 1675 3575
F 0 "U2" H 1675 3900 50  0000 C CNN
F 1 "TPS560200" H 1675 3809 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 1725 3325 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/tps560200.pdf" H 1425 3225 50  0001 C CNN
	1    1675 3575
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5F73C3DF
P 2600 4025
F 0 "R2" H 2670 4071 50  0000 L CNN
F 1 "20k" H 2670 3980 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2530 4025 50  0001 C CNN
F 3 "~" H 2600 4025 50  0001 C CNN
	1    2600 4025
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5F73C807
P 2600 3625
F 0 "R1" H 2670 3671 50  0000 L CNN
F 1 "61k9" H 2670 3580 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2530 3625 50  0001 C CNN
F 3 "~" H 2600 3625 50  0001 C CNN
	1    2600 3625
	1    0    0    -1  
$EndComp
Wire Wire Line
	1975 3825 2600 3825
$Comp
L power:GND #PWR04
U 1 1 5F73CDEA
P 2600 4300
F 0 "#PWR04" H 2600 4050 50  0001 C CNN
F 1 "GND" H 2605 4127 50  0000 C CNN
F 2 "" H 2600 4300 50  0001 C CNN
F 3 "" H 2600 4300 50  0001 C CNN
	1    2600 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5F73D146
P 5100 4625
F 0 "#PWR07" H 5100 4375 50  0001 C CNN
F 1 "GND" H 5105 4452 50  0000 C CNN
F 2 "" H 5100 4625 50  0001 C CNN
F 3 "" H 5100 4625 50  0001 C CNN
	1    5100 4625
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F73D4CB
P 1675 4000
F 0 "#PWR03" H 1675 3750 50  0001 C CNN
F 1 "GND" H 1680 3827 50  0000 C CNN
F 2 "" H 1675 4000 50  0001 C CNN
F 3 "" H 1675 4000 50  0001 C CNN
	1    1675 4000
	1    0    0    -1  
$EndComp
NoConn ~ 1375 3675
$Comp
L power:+BATT #PWR01
U 1 1 5F73EA7B
P 650 3475
F 0 "#PWR01" H 650 3325 50  0001 C CNN
F 1 "+BATT" H 665 3648 50  0000 C CNN
F 2 "" H 650 3475 50  0001 C CNN
F 3 "" H 650 3475 50  0001 C CNN
	1    650  3475
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR06
U 1 1 5F73F1ED
P 5100 2675
F 0 "#PWR06" H 5100 2525 50  0001 C CNN
F 1 "+3.3V" H 5115 2848 50  0000 C CNN
F 2 "" H 5100 2675 50  0001 C CNN
F 3 "" H 5100 2675 50  0001 C CNN
	1    5100 2675
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR05
U 1 1 5F74116D
P 3350 3175
F 0 "#PWR05" H 3350 3025 50  0001 C CNN
F 1 "+3.3V" H 3365 3348 50  0000 C CNN
F 2 "" H 3350 3175 50  0001 C CNN
F 3 "" H 3350 3175 50  0001 C CNN
	1    3350 3175
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2675 5100 2825
Wire Wire Line
	5100 4525 5100 4625
Wire Wire Line
	2600 4175 2600 4300
Wire Wire Line
	1675 3875 1675 4000
Wire Wire Line
	1375 3475 1025 3475
$Comp
L Device:C C2
U 1 1 5F7422D7
P 3050 3700
F 0 "C2" H 3165 3746 50  0000 L CNN
F 1 "10uF" H 3165 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 3088 3550 50  0001 C CNN
F 3 "~" H 3050 3700 50  0001 C CNN
	1    3050 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5F748772
P 1025 3675
F 0 "C1" H 1140 3721 50  0000 L CNN
F 1 "10uF" H 1140 3630 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 1063 3525 50  0001 C CNN
F 3 "~" H 1025 3675 50  0001 C CNN
	1    1025 3675
	1    0    0    -1  
$EndComp
Wire Wire Line
	1025 3525 1025 3475
Connection ~ 1025 3475
Wire Wire Line
	1025 3475 800  3475
$Comp
L power:GND #PWR02
U 1 1 5F74A7AE
P 1025 4000
F 0 "#PWR02" H 1025 3750 50  0001 C CNN
F 1 "GND" H 1030 3827 50  0000 C CNN
F 2 "" H 1025 4000 50  0001 C CNN
F 3 "" H 1025 4000 50  0001 C CNN
	1    1025 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1025 3825 1025 3925
Wire Wire Line
	1975 3475 2150 3475
Wire Wire Line
	3050 3475 3050 3550
Wire Wire Line
	3050 3475 3250 3475
Wire Wire Line
	3350 3475 3350 3175
Connection ~ 3050 3475
Wire Wire Line
	2600 3475 3050 3475
Wire Wire Line
	1975 3675 1975 3825
Wire Wire Line
	2600 3775 2600 3825
Connection ~ 2600 3825
Wire Wire Line
	2600 3825 2600 3875
$Comp
L Device:L L1
U 1 1 5F756F80
P 2300 3475
F 0 "L1" V 2119 3475 50  0000 C CNN
F 1 "10uH 560mA" V 2210 3475 50  0000 C CNN
F 2 "Inductor_SMD:L_6.3x6.3_H3" H 2300 3475 50  0001 C CNN
F 3 "~" H 2300 3475 50  0001 C CNN
	1    2300 3475
	0    1    1    0   
$EndComp
Wire Wire Line
	2450 3475 2600 3475
Connection ~ 2600 3475
$Comp
L power:GND #PWR08
U 1 1 5F758CB0
P 3050 3975
F 0 "#PWR08" H 3050 3725 50  0001 C CNN
F 1 "GND" H 3055 3802 50  0000 C CNN
F 2 "" H 3050 3975 50  0001 C CNN
F 3 "" H 3050 3975 50  0001 C CNN
	1    3050 3975
	1    0    0    -1  
$EndComp
Wire Wire Line
	3050 3850 3050 3925
$Comp
L power:GND #PWR010
U 1 1 5F7633A4
P 6575 3700
F 0 "#PWR010" H 6575 3450 50  0001 C CNN
F 1 "GND" H 6580 3527 50  0000 C CNN
F 2 "" H 6575 3700 50  0001 C CNN
F 3 "" H 6575 3700 50  0001 C CNN
	1    6575 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 3425 6175 3425
Wire Wire Line
	6475 3425 6575 3425
Wire Wire Line
	6000 3625 6575 3625
Wire Wire Line
	6575 3425 6575 3625
Connection ~ 6575 3625
Wire Wire Line
	6575 3625 6575 3700
$Comp
L Device:R R4
U 1 1 5F768A43
P 3800 3675
F 0 "R4" H 3870 3721 50  0000 L CNN
F 1 "2k2" H 3870 3630 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3730 3675 50  0001 C CNN
F 3 "~" H 3800 3675 50  0001 C CNN
	1    3800 3675
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR09
U 1 1 5F769336
P 3800 3425
F 0 "#PWR09" H 3800 3275 50  0001 C CNN
F 1 "+3.3V" H 3815 3598 50  0000 C CNN
F 2 "" H 3800 3425 50  0001 C CNN
F 3 "" H 3800 3425 50  0001 C CNN
	1    3800 3425
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 3425 3800 3525
Wire Wire Line
	3800 3825 4200 3825
NoConn ~ 4200 3925
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5F770C43
P 1575 1725
F 0 "J1" H 1683 2006 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1683 1915 50  0000 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x04_P2.00mm_Vertical" H 1575 1725 50  0001 C CNN
F 3 "~" H 1575 1725 50  0001 C CNN
	1    1575 1725
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5F771FD6
P 6575 1725
F 0 "J2" H 6547 1749 50  0000 R CNN
F 1 "Conn_01x03_Male" H 6547 1658 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6575 1725 50  0001 C CNN
F 3 "~" H 6575 1725 50  0001 C CNN
	1    6575 1725
	-1   0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR0101
U 1 1 5F77391F
P 6375 1625
F 0 "#PWR0101" H 6375 1475 50  0001 C CNN
F 1 "+BATT" H 6390 1798 50  0000 C CNN
F 2 "" H 6375 1625 50  0001 C CNN
F 3 "" H 6375 1625 50  0001 C CNN
	1    6375 1625
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5F773FDF
P 6375 1825
F 0 "#PWR0102" H 6375 1575 50  0001 C CNN
F 1 "GND" H 6380 1652 50  0000 C CNN
F 2 "" H 6375 1825 50  0001 C CNN
F 3 "" H 6375 1825 50  0001 C CNN
	1    6375 1825
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR0103
U 1 1 5F7747A9
P 1775 1625
F 0 "#PWR0103" H 1775 1475 50  0001 C CNN
F 1 "+BATT" H 1790 1798 50  0000 C CNN
F 2 "" H 1775 1625 50  0001 C CNN
F 3 "" H 1775 1625 50  0001 C CNN
	1    1775 1625
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5F774C96
P 1775 1925
F 0 "#PWR0104" H 1775 1675 50  0001 C CNN
F 1 "GND" H 1780 1752 50  0000 C CNN
F 2 "" H 1775 1925 50  0001 C CNN
F 3 "" H 1775 1925 50  0001 C CNN
	1    1775 1925
	1    0    0    -1  
$EndComp
Wire Wire Line
	1775 1825 4025 1825
Wire Wire Line
	4025 1825 4025 3625
Wire Wire Line
	4025 3625 4200 3625
Wire Wire Line
	4200 3525 4100 3525
Wire Wire Line
	4100 3525 4100 1725
Wire Wire Line
	4100 1725 1775 1725
Wire Wire Line
	6000 3725 6150 3725
Wire Wire Line
	6150 3725 6150 1725
Wire Wire Line
	6150 1725 6375 1725
Text Label 2050 3475 0    50   ~ 0
pind
Text Label 3975 3825 0    50   ~ 0
CH_PD
Text Label 2850 1825 0    50   ~ 0
RX
Text Label 2850 1725 0    50   ~ 0
TX
Text Label 6150 2500 0    50   ~ 0
LEDOUT
$Comp
L Device:C C3
U 1 1 5F788077
P 800 3675
F 0 "C3" H 915 3721 50  0000 L CNN
F 1 "10uF" H 915 3630 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 838 3525 50  0001 C CNN
F 3 "~" H 800 3675 50  0001 C CNN
	1    800  3675
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5F78830E
P 3250 3700
F 0 "C4" H 3365 3746 50  0000 L CNN
F 1 "10uF" H 3365 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 3288 3550 50  0001 C CNN
F 3 "~" H 3250 3700 50  0001 C CNN
	1    3250 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  3525 800  3475
Connection ~ 800  3475
Wire Wire Line
	800  3475 650  3475
Wire Wire Line
	800  3825 800  3925
Wire Wire Line
	800  3925 1025 3925
Connection ~ 1025 3925
Wire Wire Line
	1025 3925 1025 4000
Wire Wire Line
	3250 3550 3250 3475
Connection ~ 3250 3475
Wire Wire Line
	3250 3475 3350 3475
Wire Wire Line
	3250 3850 3250 3925
Wire Wire Line
	3250 3925 3050 3925
Connection ~ 3050 3925
Wire Wire Line
	3050 3925 3050 3975
$EndSCHEMATC
